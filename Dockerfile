# Kita akan menggunakan docker image official golang
FROM golang:1.20-alpine AS builder

# Menjadikan folder /app sebagai working directory
# Semua command berikutnya akan dijalankan dalam folder ini
WORKDIR /app

# Menyalin semua folder `.` ditempat dimana kita menjalankan docker build
# kedalam docker image pada folder /app
COPY . /app/

# Melakukan go build dalam docker image
RUN GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -o golang-cicd main.go

# Mendeklarasikan step final
# Tetap menggunakan docker image official golang
FROM golang:1.20-alpine

# Menyalin hasil build dari step sebelumnya
# ke directory /usr/local/bin
COPY --from=builder ./app/ /usr/local/bin

# Mendeklarasikan command yang harus dijalankan ketika
# kita menjalankan perintah docker run
ENTRYPOINT ["golang-cicd"]